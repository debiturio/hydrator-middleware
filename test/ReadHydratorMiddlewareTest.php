<?php

namespace Debiturio\HydratorMiddlewareTest;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Factory\ReadHydratorFactory;
use Debiturio\HydratorMiddleware\Property\PropertiesHydrator;
use Debiturio\HydratorMiddleware\ReadCollectionHydratorMiddleware;
use Debiturio\HydratorMiddlewareTest\TestModel\ClassMethodHydratorTestClass;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadHydratorMiddlewareTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param object $data
     * @param array $input
     * @param array|string $properties
     * @param string $key
     * @return void
     * @throws \ReflectionException
     */
    public function testProcess(object $data, array $input, array|string $properties, string $key)
    {
        $hydratorFactory = $this->createMock(ReadHydratorFactory::class);
        $propertiesHydrator = $this->createMock(PropertiesHydrator::class);

        $hydrator = $this->createMock(HydratorInterface::class);
        $hydratorFactory->expects($this->once())
            ->method('getHydrator')
            ->with(get_class($data))
            ->willReturn($hydrator);

        $hydrator->expects($this->once())->method('extract')->with($data)->willReturn(['foo' => 'bar']);


        $handler = $this->createMock(RequestHandlerInterface::class);

        $middleware = new ReadCollectionHydratorMiddleware($hydratorFactory, $propertiesHydrator, CaseStyle::CAMEL_CASE, $key);

        $hydrationResult = [...$input, 'data' => ['foo' => 'bar']];
        $requestResult = $this->createMock(ServerRequestInterface::class);
        $requestResult->expects($this->once())->method('getParsedBody')->willReturn($hydrationResult);

        $request = $this->createMock(ServerRequestInterface::class);
        $request->expects($this->once())->method('getParsedBody')->willReturn($input);
        $request->expects($this->once())
            ->method('withParsedBody')
            ->willReturn($requestResult);

        $request->method('getQueryParams')->willReturn(['properties' => $properties]);

        $handler->expects($this->once())
            ->method('handle')
            ->with($this->callback(function (ServerRequestInterface $serverRequest) use ($hydrationResult){
                $this->assertEquals($hydrationResult, $serverRequest->getParsedBody());
                return true;
            }));

        $middleware->process($request, $handler);

    }

    public function dataProvider()
    {
        return [
            [
                $data = $this->createMock(ClassMethodHydratorTestClass::class),
                [
                    'data' => $data,
                    'meta' => [
                        'foo'
                    ]
                ],
                'foo',
                'data'
            ],
            [
                $data = $this->createMock(ClassMethodHydratorTestClass::class),
                [
                    'items' => $data,
                    'meta' => [
                        'foo'
                    ]
                ],
                ['foo', 'bar'],
                'items'
            ]
        ];
    }
}
