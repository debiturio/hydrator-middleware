<?php

namespace Debiturio\HydratorMiddlewareTest;

use Debiturio\HydratorMiddleware\AbstractHydrateMiddleware;
use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Debiturio\HydratorMiddlewareTest\TestModel\ValueObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

class AbstractHydrateMiddlewareTest extends TestCase
{

    /**
     * @dataProvider getClassNameProvider
     * @param array $config
     * @param string $path
     * @param string $method
     * @param string $expectedClassName
     * @return void
     * @throws \Exception
     */
    public function testGetClassName(array $config, string $path, string $method, string $expectedClassName)
    {
        $middleware = $this->getMockForAbstractClass(
            AbstractHydrateMiddleware::class,
            [
                $this->createStub(MutateHydratorFactory::class),
                $config
            ]
        );

        $request = $this->createStub(ServerRequestInterface::class);
        $request->method('getMethod')->willReturn($method);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);

        $request->method('getUri')->willReturn($uri);

        $this->assertEquals($expectedClassName, $middleware->getClassName($request));
    }

    public function getClassNameProvider()
    {
        $config = [
            '/ping' => [
                'post' => NestedObject::class,
                'put' => ValueObject::class
            ],
            '/test' => ValueObject::class
        ];

        return [
            [
                $config,
                '/ping',
                'post',
                NestedObject::class
            ],
            [
                $config,
                '/ping',
                'put',
                ValueObject::class
            ],
            [
                $config,
                '/test',
                'put',
                ValueObject::class
            ]
        ];
    }

    /**
     * @dataProvider dataIsCollectionProvider
     * @param array $data
     * @param bool $expectation
     * @return void
     */
    public function testDataIsCollection(array $data, bool $expectation)
    {
        $middleware = $this->getMockForAbstractClass(
            AbstractHydrateMiddleware::class,
            [
                $this->createStub(MutateHydratorFactory::class),
                []
            ]
        );

        $this->assertEquals($expectation, $middleware->dataIsCollection($data));

    }

    public function dataIsCollectionProvider()
    {
        return [
            [
                [
                    ['foo' => 'bar'],
                    ['baz' => 'boo']
                ],
                true
            ],
            [
                [
                    'foo' => 'bar',
                    'baz' => 'foo'
                ],
                false
            ]
        ];
    }
}
