<?php

namespace Debiturio\HydratorMiddlewareTest\Factory;

use Debiturio\HydratorMiddleware\Factory\AbstractHydratorFactory;
use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Laminas\Hydrator\Strategy\StrategyInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Uuid;

class AbstractHydratorFactoryTest extends TestCase
{
    public function testGetCollectionItemClassNames()
    {
        /** @var AbstractHydratorFactory $factory */
        $factory = $this->getMockForAbstractClass(
            AbstractHydratorFactory::class,
            [
                $this->createStub(ContainerInterface::class),
                $this->createStub(ExcludedPropertyCollection::class),
                [
                    'TestObject' => [
                        'users' => self::class,
                        'ids' => Uuid::class
                    ]
                ],
                []
            ]
        );

        $this->assertEquals(
            Uuid::class,
            $factory->getCollectionItemClassNames('TestObject', 'ids')
        );

        $this->assertEquals(
            self::class,
            $factory->getCollectionItemClassNames('TestObject', 'users')
        );

    }

    /**
     * @dataProvider dataProviderTestGetConfiguredStrategy
     * @param string $typeName
     * @param array $configuration
     * @param StrategyInterface|null $strategy
     * @return void
     */
    public function testGetConfiguredStrategy(string            $typeName,
                                              array             $configuration,
                                              StrategyInterface $strategy = null)
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->method('has')->willReturn(!!$strategy);
        $container->method('get')->willReturn($strategy);

        $rf = $this->createStub(\ReflectionNamedType::class);
        $rf->method('getName')->willReturn($typeName);

        /** @var AbstractHydratorFactory $factory */
        $factory = $this->getMockForAbstractClass(
            AbstractHydratorFactory::class,
            [
                $container,
                $this->createStub(ExcludedPropertyCollection::class),
                [],
                $configuration
            ]
        );

        $this->assertEquals($strategy, $factory->getConfiguredStrategy($rf));
    }

    public function dataProviderTestGetConfiguredStrategy(): array
    {
        return [
            [
                'Foo',
                [
                    'Foo' => 'FooStrategy'
                ],
                $this->createStub(StrategyInterface::class)
            ],
            [
                'bar',
                [],
                null
            ]
        ];
    }

}
