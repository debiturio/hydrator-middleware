<?php

namespace Debiturio\HydratorMiddlewareTest\Factory;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Debiturio\HydratorMiddlewareTest\TestModel\ClassMethodHydratorTestClass;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Debiturio\HydratorMiddlewareTest\TestModel\StringEnum;
use Debiturio\HydratorMiddlewareTest\TestModel\ValueObject;
use PHPUnit\Framework\MockObject\MockClass;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class MutateHydratorFactoryTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param array $input
     * @param string $objectClassName
     * @param object $object
     * @param CaseStyle $caseStyle
     * @param MockClass[] $repositoryMocks
     * @param object $expectedResult
     * @return void
     * @throws \ReflectionException
     */
    public function testGetHydrator(
        array $input,
        string $objectClassName,
        object $object,
        CaseStyle $caseStyle,
        array $repositoryMocks,
        object $expectedResult)
    {

        $repository = $this->createMock(ObjectRepositoryInterface::class);

        $repository
            ->expects($this->exactly(count($repositoryMocks)))
            ->method('getById')
            ->withConsecutive(
                ...array_map(
                    function ($item) {
                        return [$item['class'], $item['id']];
                    },
                    $repositoryMocks
                )
            )
            ->willReturnOnConsecutiveCalls(...array_map(function ($item) { return $item['mock']; }, $repositoryMocks));

        $factory = new MutateHydratorFactory(
            $this->createStub(ContainerInterface::class),
            $repository,
            $this->createStub(ExcludedPropertyCollection::class),
            [
                ClassMethodHydratorTestClass::class => [
                    'arrayIteratorWithNestedObject' => [
                        NestedObject::class
                    ]
                ]
            ],
            [],
            [],
            null
        );

        $hydrator = $factory->getHydrator($objectClassName, null);

        $this->assertEquals($expectedResult, $hydrator->hydrate($input, $object));

    }

    public function dataProvider()
    {
        $createdAt = date('c');

        return [
            [
                [
                    'id' => $id = Uuid::uuid4()->toString(),
                    'stringValue' => 'Lorem Ipsum',
                    'numberValue' => 122,
                    'arrayValue' => ['foo' => 'bar'],
                    'nestedObject' => $nestedId = Uuid::uuid4()->toString(),
                    'arrayIteratorWithNestedObject' => [
                        $iteratorIdOne = Uuid::uuid4()->toString(),
                        $iteratorIdTwo = Uuid::uuid4()->toString(),
                        $iteratorIdThree = Uuid::uuid4()->toString(),
                        $iteratorIdFour = Uuid::uuid4()->toString()
                    ],
                    'enum' => 'foo',
                    'scalarUnionTypes' => 'test',
                    'object' => ['name' => 'foo'],
                    'createdAt' => $createdAt
                ],
                ClassMethodHydratorTestClass::class,
                (new \ReflectionClass(ClassMethodHydratorTestClass::class))->newInstanceWithoutConstructor(),
                CaseStyle::CAMEL_CASE,
                $mocks = [
                    [
                        'id' => $nestedId,
                        'mock' => $this->getIdMock(NestedObject::class, $nestedId),
                        'class' => NestedObject::class
                    ],
                    [
                        'id' => $iteratorIdOne,
                        'mock' => $this->getIdMock(NestedObject::class, $iteratorIdOne),
                        'class' => NestedObject::class
                    ],
                    [
                        'id' => $iteratorIdTwo,
                        'mock' => $this->getIdMock(NestedObject::class, $iteratorIdTwo),
                        'class' => NestedObject::class
                    ],
                    [
                        'id' => $iteratorIdThree,
                        'mock' => $this->getIdMock(NestedObject::class, $iteratorIdThree),
                        'class' => NestedObject::class
                    ],
                    [
                        'id' => $iteratorIdFour,
                        'mock' => $this->getIdMock(NestedObject::class, $iteratorIdFour),
                        'class' => NestedObject::class
                    ]
                ],
                new ClassMethodHydratorTestClass(
                    Uuid::fromString($id),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    StringEnum::FOO,
                    $mocks[0]['mock'],
                    new \ArrayIterator(array_slice(array_map(function ($item) { return $item['mock']; }, $mocks), 1)),
                    'test',
                    new ValueObject('foo'),
                    new \DateTime($createdAt)
                )
            ],
            [
                [
                    'stringValue' => 'Lorem Ipsum dolor',
                    'numberValue' => 25,
                ],
                ClassMethodHydratorTestClass::class,
                new ClassMethodHydratorTestClass(
                    Uuid::fromString($id),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    StringEnum::FOO,
                    new NestedObject(Uuid::fromString($id), 'name', 'c/o'),
                    new \ArrayIterator(),
                    'test',
                    null,
                    new \DateTime($createdAt)
                ),
                CaseStyle::CAMEL_CASE,
                [],
                new ClassMethodHydratorTestClass(
                    Uuid::fromString($id),
                    'Lorem Ipsum dolor',
                    25,
                    ['foo' => 'bar'],
                    StringEnum::FOO,
                    new NestedObject(Uuid::fromString($id), 'name', 'c/o'),
                    new \ArrayIterator(),
                    'test',
                    null,
                    new \DateTime($createdAt)
                )
            ]
        ];
    }

    public function getIdMock(string $class, UuidInterface|string $id)
    {
        $mock = $this->createMock($class);
        $mock->method('getId')->willReturn(is_string($id) ? Uuid::fromString($id) : $id);
        return $mock;
    }
}
