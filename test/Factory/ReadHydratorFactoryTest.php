<?php

namespace Debiturio\HydratorMiddlewareTest\Factory;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Factory\ReadHydratorFactory;
use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use Debiturio\HydratorMiddlewareTest\TestModel\ClassMethodHydratorTestClass;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Debiturio\HydratorMiddlewareTest\TestModel\ValueObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ReadHydratorFactoryTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param object $object
     * @param PropertyCollection $properties
     * @param CaseStyle $caseStyle
     * @param ExcludedPropertyCollection $excludedProperties
     * @param array $expectedResult
     * @return void
     * @throws \ReflectionException
     */
    public function testGetHydrator(
        object $object,
        PropertyCollection $properties,
        CaseStyle $caseStyle,
        ExcludedPropertyCollection $excludedProperties,
        array $expectedResult
    ) {

        $factory = new ReadHydratorFactory($excludedProperties, [
            ClassMethodHydratorTestClass::class => [
                'arrayIteratorWithNestedObject' => [
                    NestedObject::class
                ]
            ]
        ]);

        $hydrator = $factory->getHydrator(
            get_class($object),
            $properties,
            $caseStyle
        );

        $this->assertEquals($expectedResult, $hydrator->extract($object));

    }

    public function dataProvider()
    {
        return [
            [
                new ClassMethodHydratorTestClass(
                    $id = Uuid::uuid4(),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    new NestedObject($nestedId = Uuid::uuid4(), 'Bob', 'c/o Alice'),
                    new \ArrayIterator([
                        new NestedObject($iteratorIdOne = Uuid::uuid4(),'John', 'c/o Yoko'),
                        new NestedObject($iteratorIdTwo = Uuid::uuid4(),'Paul', 'c/o Uncle Albert'),
                        new NestedObject($iteratorIdThree = Uuid::uuid4(),'Ringo', 'c/o Victory'),
                        new NestedObject($iteratorIdFour = Uuid::uuid4(),'George', 'c/o Shangrila')
                    ]),
                    'test',
                    new ValueObject('My Name'),
                    $date = new \DateTime()
                ),
                new PropertyCollection(new Property('*')),
                CaseStyle::CAMEL_CASE,
                new ExcludedPropertyCollection(
                    new PropertyCollection(
                        new Property('abc')
                    )
                ),
                [
                    'id' => $id->toString(),
                    'stringValue' => 'Lorem Ipsum',
                    'numberValue' => 122,
                    'arrayValue' => ['foo' => 'bar'],
                    'nestedObject' => $nestedId->toString(),
                    'arrayIteratorWithNestedObject' => [
                        $iteratorIdOne->toString(),
                        $iteratorIdTwo->toString(),
                        $iteratorIdThree->toString(),
                        $iteratorIdFour->toString()
                    ],
                    'scalarUnionTypes' => 'test',
                    'object' => null,
                    'createdAt' => $date->format('c')
                ]
            ],
            [
                new ClassMethodHydratorTestClass(
                    $id = Uuid::uuid4(),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    new NestedObject($nestedId = Uuid::uuid4(), 'Bob', 'c/o Alice'),
                    new \ArrayIterator([
                        new NestedObject($iteratorIdOne = Uuid::uuid4(),'John', 'c/o Yoko'),
                        new NestedObject($iteratorIdTwo = Uuid::uuid4(),'Paul', 'c/o Uncle Albert'),
                        new NestedObject($iteratorIdThree = Uuid::uuid4(),'Ringo', 'c/o Victory'),
                        new NestedObject($iteratorIdFour = Uuid::uuid4(),'George', 'c/o Shangrila')
                    ]),
                    'test',
                    new ValueObject('My Name'),
                    $date = new \DateTime()
                ),
                new PropertyCollection(
                    new Property('*'),
                    new Property(
                        'object',
                        new PropertyCollection(
                            new Property('name')
                        )
                    )
                ),
                CaseStyle::CAMEL_CASE,
                new ExcludedPropertyCollection(
                    new PropertyCollection(
                        new Property('abc'),
                        new Property('numberValue'),
                        new Property('arrayIteratorWithNestedObject'),
                        new Property('scalarUnionTypes')
                    )
                ),
                [
                    'id' => $id->toString(),
                    'stringValue' => 'Lorem Ipsum',
                    'arrayValue' => ['foo' => 'bar'],
                    'nestedObject' => $nestedId->toString(),
                    'object' => [
                        'name' => 'My Name'
                    ],
                    'createdAt' => $date->format('c')
                ]
            ],
            [
                new ClassMethodHydratorTestClass(
                    $id = Uuid::uuid4(),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    new NestedObject($nestedId = Uuid::uuid4(), 'Bob', 'c/o Alice'),
                    new \ArrayIterator([
                        new NestedObject($iteratorIdOne = Uuid::uuid4(),'John', 'c/o Yoko'),
                        new NestedObject($iteratorIdTwo = Uuid::uuid4(),'Paul', 'c/o Uncle Albert'),
                        new NestedObject($iteratorIdThree = Uuid::uuid4(),'Ringo', 'c/o Victory'),
                        new NestedObject($iteratorIdFour = Uuid::uuid4(),'George', 'c/o Shangrila')
                    ]),
                    'test',
                    new ValueObject('My Name'),
                    $date = new \DateTime()
                ),
                new PropertyCollection(
                    new Property('*'),
                    new Property(
                        'object',
                        new PropertyCollection(
                            new Property('name')
                        )
                    )
                ),
                CaseStyle::SNAKE_CASE,
                new ExcludedPropertyCollection(
                    new PropertyCollection(
                        new Property('abc'),
                        new Property('numberValue'),
                        new Property('arrayIteratorWithNestedObject'),
                        new Property('object'),
                        new Property('nestedObject'),
                        new Property('scalarUnionTypes')
                    )
                ),
                [
                    'id' => $id->toString(),
                    'string_value' => 'Lorem Ipsum',
                    'array_value' => ['foo' => 'bar'],
                    'created_at' => $date->format('c')
                ]
            ],
            [
                new ClassMethodHydratorTestClass(
                    $id = Uuid::uuid4(),
                    'Lorem Ipsum',
                    122,
                    ['foo' => 'bar'],
                    new NestedObject($nestedId = Uuid::uuid4(), 'Bob', 'c/o Alice'),
                    new \ArrayIterator([
                        new NestedObject($iteratorIdOne = Uuid::uuid4(),'John', 'c/o Yoko'),
                        new NestedObject($iteratorIdTwo = Uuid::uuid4(),'Paul', 'c/o Uncle Albert'),
                        new NestedObject($iteratorIdThree = Uuid::uuid4(),'Ringo', 'c/o Victory'),
                        new NestedObject($iteratorIdFour = Uuid::uuid4(),'George', 'c/o Shangrila')
                    ]),
                    'test',
                    new ValueObject('My Name'),
                    $date = new \DateTime()
                ),
                new PropertyCollection(
                    new Property('*'),
                    new Property(
                        'arrayIteratorWithNestedObject',
                        new PropertyCollection(
                            new Property('id'),
                            new Property('name')
                        )
                    )
                ),
                CaseStyle::CAMEL_CASE,
                new ExcludedPropertyCollection(
                    new PropertyCollection(
                        new Property('abc')
                    )
                ),
                [
                    'id' => $id->toString(),
                    'stringValue' => 'Lorem Ipsum',
                    'numberValue' => 122,
                    'arrayValue' => ['foo' => 'bar'],
                    'nestedObject' => $nestedId->toString(),
                    'arrayIteratorWithNestedObject' => [
                        [
                            'id' => $iteratorIdOne->toString(),
                            'name' => 'John'
                        ],
                        [
                            'id' => $iteratorIdTwo->toString(),
                            'name' => 'Paul'
                        ],
                        [
                            'id' => $iteratorIdThree->toString(),
                            'name' => 'Ringo'
                        ],
                        [
                            'id' => $iteratorIdFour->toString(),
                            'name' => 'George'
                        ]
                    ],
                    'scalarUnionTypes' => 'test',
                    'object' => null,
                    'createdAt' => $date->format('c')
                ]
            ],
        ];
    }
}
