<?php

namespace Debiturio\HydratorMiddlewareTest\Property;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use Debiturio\HydratorMiddleware\Property\PropertyFilter;
use PHPUnit\Framework\TestCase;

class PropertyFilterTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param ?PropertyCollection $propertyCollection
     * @param CaseStyle $caseStyle
     * @param ?PropertyCollection $excludedCollection
     * @param array $trueExpectedInput
     * @param array $falseExpectedInput
     * @return void
     */
    public function testFilter(
        CaseStyle $caseStyle,
        PropertyCollection $propertyCollection = null,
        PropertyCollection $excludedCollection = null,
        array $trueExpectedInput = [],
        array $falseExpectedInput = []
    ) {
        $filter = new PropertyFilter($caseStyle, $propertyCollection, $excludedCollection);

        foreach ($trueExpectedInput as $method) {
            $this->assertTrue($filter->filter($method));
        }

        foreach ($falseExpectedInput as $method) {
            $this->assertFalse($filter->filter($method));
        }
    }

    public function dataProvider(): array
    {
        return [
            [
                CaseStyle::SNAKE_CASE,
                new PropertyCollection(
                    new Property('personal_address_addition'),
                    new Property('name')
                ),
                new PropertyCollection(
                    new Property('unallowed'),
                    new Property('not_allowed')
                ),
                [
                    'getPersonalAddressAddition',
                    'getName'
                ],
                [
                    'getUnallowed',
                    'getNotAllowed'
                ]
            ],
            [
                CaseStyle::CAMEL_CASE,
                new PropertyCollection(
                    new Property('personalAddressAddition'),
                    new Property('name')
                ),
                new PropertyCollection(
                    new Property('unallowed'),
                    new Property('notAllowed')
                ),
                [
                    'getPersonalAddressAddition',
                    'getName'
                ],
                [
                    'getUnallowed',
                    'getNotAllowed'
                ]
            ],
            [
                CaseStyle::CAMEL_CASE,
                null,
                new PropertyCollection(
                    new Property('unallowed'),
                    new Property('notAllowed')
                ),
                [
                    'getPersonalAddressAddition',
                    'getName'
                ],
                [
                    'getUnallowed',
                    'getNotAllowed'
                ]
            ],
            [
                CaseStyle::CAMEL_CASE,
                null,
                null,
                [
                    'getPersonalAddressAddition',
                    'getName'
                ]
            ]
        ];
    }
}
