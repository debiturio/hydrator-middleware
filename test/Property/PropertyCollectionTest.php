<?php

namespace Debiturio\HydratorMiddlewareTest\Property;

use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use PHPUnit\Framework\TestCase;

class PropertyCollectionTest extends TestCase
{

    public function test__construct()
    {
        $collection = new PropertyCollection(new Property('foo'), new Property('bar'), new Property('baz'));
        foreach ($collection as $item) $this->assertInstanceOf(Property::class, $item);

        $this->assertCount(3, $collection);
    }

    public function testGetPropertyByKey()
    {
        $collection = new PropertyCollection(new Property('foo'), new Property('bar'), new Property('*'));
        $this->assertEquals(new Property('bar'), $collection->getPropertyByKey('bar'));
    }

    public function testIsEmpty()
    {
        $empty = new PropertyCollection();
        $this->assertTrue($empty->isEmpty());

        $filled = new PropertyCollection(new Property('foo'));
        $this->assertFalse($filled->isEmpty());
    }
}
