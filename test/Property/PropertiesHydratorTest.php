<?php

namespace Debiturio\HydratorMiddlewareTest\Property;

use Debiturio\HydratorMiddleware\Property\PropertiesHydrator;
use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use PHPUnit\Framework\TestCase;

class PropertiesHydratorTest extends TestCase
{
    /**
     * @dataProvider extractDataProvider
     * @param Property[] $expectedResult
     * @param PropertyCollection $data
     * @return void
     */
    public function testExtract(array $expectedResult, PropertyCollection $data): void
    {
        $hydrator = new PropertiesHydrator();
        $this->assertEquals($expectedResult, $hydrator->extract($data));
    }

    /**
     * @dataProvider hydrateDataProvider
     * @param array $data
     * @param PropertyCollection $expectedResult
     * @return void
     */
    public function testHydrate(array $data, PropertyCollection $expectedResult): void
    {
        $hydrator = new PropertiesHydrator();
        $this->assertEquals($expectedResult, $hydrator->hydrate($data));
    }

    public function dataProvider(): array
    {
        return [
            [
                [],
                new PropertyCollection()
            ],
            [
                [
                    'foo.bar.baz',
                    'foo.bar.bon',
                    'foo.bar.bob',
                    'baz.foo',
                    'abc'
                ],
                new PropertyCollection(
                    new Property(
                        'foo',
                        new PropertyCollection(
                            new Property('bar',
                                new PropertyCollection(
                                    new Property('baz'),
                                    new Property('bon'),
                                    new Property('bob')
                                )
                            )
                        )
                    ),
                    new Property(
                        'baz',
                        new PropertyCollection(
                            new Property('foo')
                        )
                    ),
                    new Property('abc')
                )
            ],
            [
                [
                    'abc.*'
                ],
                new PropertyCollection(
                    new Property(
                        'abc',
                        new PropertyCollection(
                            new Property('*')
                        )
                    )
                )
            ],
            [
                [
                    'foo'
                ],
                new PropertyCollection(
                    new Property('foo')
                )
            ]
        ];
    }

    public function extractDataProvider(): array
    {
        return [
            ...$this->dataProvider(),
            [
                [
                    '*.*.*',
                    'foo.bar.bob.now'
                ],
                new PropertyCollection(
                    new Property('*',
                        new PropertyCollection(
                            new Property(
                                '*',
                                new PropertyCollection(new Property('*'))
                            )
                        )
                    ),
                    new Property(
                        'foo',
                        new PropertyCollection(
                            new Property(
                                '*',
                                new PropertyCollection(new Property('*'))
                            ),
                            new Property('bar',
                                new PropertyCollection(
                                    new Property('*'),
                                    new Property('baz'),
                                    new Property('bon'),
                                    new Property(
                                        'bob',
                                        new PropertyCollection(new Property('now'))
                                    )
                                )
                            )
                        )
                    ),
                    new Property(
                        'baz',
                        new PropertyCollection(
                            new Property('*',
                                new PropertyCollection(new Property('*'))
                            ),
                            new Property('foo', new PropertyCollection(new Property('*'))
                            )
                        )
                    ),
                    new Property(
                        'abc',
                        new PropertyCollection(
                            new Property(
                                '*',
                                new PropertyCollection(new Property('*'))
                            )
                        )
                    )
                )
            ]
        ];
    }

    public function hydrateDataProvider(): array
    {
        return [
            ...$this->dataProvider(),
            [
                [
                    'foo.bar.baz',
                    'foo.bar.bon',
                    'foo.bar.bob',
                    'baz.foo',
                    'abc',
                    'foo'
                ],
                new PropertyCollection(
                    new Property(
                        'foo',
                        new PropertyCollection(
                            new Property('bar',
                                new PropertyCollection(
                                    new Property('baz'),
                                    new Property('bon'),
                                    new Property('bob')
                                )
                            )
                        )
                    ),
                    new Property(
                        'baz',
                        new PropertyCollection(
                            new Property('foo')
                        )
                    ),
                    new Property('abc')
                )
            ],
            [
                [
                    '*.*.*',
                    'foo.bar.baz',
                    'foo.bar.bon',
                    'foo.bar.bob.now',
                    'baz.foo',
                    'abc',
                    'foo'
                ],
                new PropertyCollection(
                    new Property('*',
                        new PropertyCollection(
                            new Property('*',
                                new PropertyCollection(new Property('*'))
                            ),
                        )
                    ),
                    new Property(
                        'foo',
                        new PropertyCollection(
                            new Property(
                                '*',
                                new PropertyCollection(new Property('*'))
                            ),
                            new Property('bar',
                                new PropertyCollection(
                                    new Property('*'),
                                    new Property('baz'),
                                    new Property('bon'),
                                    new Property(
                                        'bob',
                                        new PropertyCollection(new Property('now'))
                                    )
                                )
                            )
                        )
                    ),
                    new Property(
                        'baz',
                        new PropertyCollection(
                            new Property('*',
                                new PropertyCollection(new Property('*'))
                            ),
                            new Property(
                                'foo',
                                new PropertyCollection(
                                    new Property('*')
                                )
                            )
                        )
                    ),
                    new Property(
                        'abc',
                        new PropertyCollection(
                            new Property(
                                '*',
                                new PropertyCollection(new Property('*'))
                            )
                        )
                    )
                )
            ],
        ];
    }
}
