<?php

namespace Debiturio\HydratorMiddlewareTest\Property;

use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use PHPUnit\Framework\TestCase;

class PropertyTest extends TestCase
{

    public function test__construct()
    {
        $propertyOne = new Property('foo', null);
        $this->assertFalse($propertyOne->hasNestedProperties());
        $this->assertEquals('foo', $propertyOne->getKey());

        $propertyTwo = new Property(
            'bar',
            $collection = new PropertyCollection(
                new Property('baz')
            )
        );

        $this->assertTrue($propertyTwo->hasNestedProperties());
        $this->assertEquals('bar', $propertyTwo->getKey());
        $this->assertEquals($collection, $propertyTwo->getNestedProperties());
    }
}
