<?php

namespace Debiturio\HydratorMiddlewareTest\Property;

use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Debiturio\HydratorMiddlewareTest\TestModel\ValueObject;
use PHPUnit\Framework\TestCase;

class ExcludedPropertyCollectionTest extends TestCase
{

    /**
     * @param array $input
     * @param array $expectedResult
     * @return void
     */
    public function testGetExcludedPropertyCollections()
    {
        $collection = ExcludedPropertyCollection::fromArray([
            'foo',
            'baz',
            NestedObject::class => [
                'bar',
                'foo'
            ],
            ValueObject::class => [
                'boo'
            ]
        ]);

        $this->assertEquals(
            new PropertyCollection(
                new Property('foo'),
                new Property('baz')
            ),
            $collection->getDefault()
        );

        $this->assertEquals(
            new PropertyCollection(
                new Property('foo'),
                new Property('baz'),
                new Property('bar')
            ),
            $collection->getByKey(NestedObject::class)
        );

        $this->assertEquals(
            new PropertyCollection(
                new Property('foo'),
                new Property('baz'),
                new Property('boo')
            ),
            $collection->getByKey(ValueObject::class)
        );
    }
}
