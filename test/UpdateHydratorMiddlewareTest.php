<?php

namespace Debiturio\HydratorMiddlewareTest;

use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Debiturio\HydratorMiddleware\UpdateHydratorMiddleware;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class UpdateHydratorMiddlewareTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param array $endpointConfig
     * @param array $input
     * @param string $method
     * @param string $path
     * @param string|null $id
     * @return void
     * @throws \Exception
     */
    public function testProcess(array $endpointConfig, array $input, string $method, string $path, string $id = null)
    {
        $request = $this->createMock(ServerRequestInterface::class);

        $hydratorResult = array_fill(
            0,
            !$id ? count($input) : 1,
            $this->createStub(NestedObject::class)
        );

        $repository = $this->createMock(ObjectRepositoryInterface::class);

        if ($id) {
            $request->method('getAttribute')->willReturn($id);
            $repository->method('getById')
                ->with(NestedObject::class, $id)
                ->willReturn($this->createStub(NestedObject::class));
        } else {
            $repository->method('getById')
                ->withConsecutive(...array_map(function (array $item) {
                    return [NestedObject::class, $item['id']];
                }, $input))
                ->willReturn($this->createStub(NestedObject::class));
        }



        $hydrator = $this->createMock(HydratorInterface::class);
        $hydrator->method('hydrate')
            ->with(
                $this->callback(function ($data) use ($id, $input) {
                    if (!$id) {
                        $this->assertTrue(in_array($data, $input));
                    } else {
                        $this->assertEquals($input, $data);
                    }
                    return true;
                }),
                $this->callback(function ($object) use ($id, $input) {
                    $this->assertInstanceOf(NestedObject::class, $object);
                    return true;
                }),
            )
            ->willReturnOnConsecutiveCalls(...$hydratorResult);

        $handler = $this->createMock(RequestHandlerInterface::class);

        $hydratorFactory = $this->createMock(MutateHydratorFactory::class);
        $hydratorFactory->method('getHydrator')->willReturn($hydrator);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($uri);

        $request->method('getParsedBody')->willReturn($input);
        $request->expects($this->once())
            ->method('withParsedBody')
            ->with($id ? $hydratorResult[0] : $hydratorResult)
            ->willReturn($resultRequest = $this->createStub(ServerRequestInterface::class));

        $handler->expects($this->once())
            ->method('handle')
            ->with($resultRequest);


        $middleware = new UpdateHydratorMiddleware($repository, $hydratorFactory, $endpointConfig);
        $middleware->process($request, $handler);
    }

    public function dataProvider()
    {
        return [
            [
                [
                    '/foo' => [
                        'post' => NestedObject::class
                    ]
                ],
                [
                    'foo' => 'bar',
                    'baz' => 'boo'
                ],
                'post',
                '/foo',
                Uuid::uuid4()->toString()
            ],
            [
                [
                    '/foo' => NestedObject::class
                ],
                [
                    [
                        'id' => Uuid::uuid4()->toString(),
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ],
                    [
                        'id' => Uuid::uuid4()->toString(),
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ],
                    [
                        'id' => Uuid::uuid4()->toString(),
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ]
                ],
                'put',
                '/foo'
            ],
        ];
    }
}
