<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddlewareTest\TestModel;

class ValueObject
{
    public function __construct(private string $name)
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}