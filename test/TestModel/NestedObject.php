<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddlewareTest\TestModel;

use Ramsey\Uuid\UuidInterface;

class NestedObject
{
    public function __construct(protected UuidInterface $id, protected string $name, protected string $personalAddressAddition)
    {
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPersonalAddressAddition(): string
    {
        return $this->personalAddressAddition;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}