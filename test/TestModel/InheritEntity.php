<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddlewareTest\TestModel;

use Ramsey\Uuid\UuidInterface;

class InheritEntity extends NestedObject
{
    public function __construct(UuidInterface $id, string $name, string $personalAddressAddition, private int $age)
    {
        parent::__construct($id, $name, $personalAddressAddition);
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }
}