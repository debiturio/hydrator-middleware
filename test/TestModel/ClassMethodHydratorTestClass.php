<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddlewareTest\TestModel;

use Ramsey\Uuid\UuidInterface;

class ClassMethodHydratorTestClass
{
    public function __construct(
        private UuidInterface $id,
        private string $stringValue,
        private int $numberValue,
        private array $arrayValue,
        private StringEnum $enum,
        private NestedObject $nestedObject,
        private \ArrayIterator $arrayIteratorWithNestedObject,
        private string|int|float $scalarUnionTypes,
        private ValueObject|NestedObject|null $object = null,
        private ?\DateTime $createdAt = null,
    ){
        if (!$this->createdAt) $this->createdAt = new \DateTime();
    }

    public function getAbc(): string
    {
        return 'abc';
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStringValue(): string
    {
        return $this->stringValue;
    }

    /**
     * @return int
     */
    public function getNumberValue(): int
    {
        return $this->numberValue;
    }

    /**
     * @return array
     */
    public function getArrayValue(): array
    {
        return $this->arrayValue;
    }

    /**
     * @return NestedObject
     */
    public function getNestedObject(): NestedObject
    {
        return $this->nestedObject;
    }

    /**
     * @return \ArrayIterator
     */
    public function getArrayIteratorWithNestedObject(): \ArrayIterator
    {
        return $this->arrayIteratorWithNestedObject;
    }

    /**
     * @return float|int|string
     */
    public function getScalarUnionTypes(): float|int|string
    {
        return $this->scalarUnionTypes;
    }

    /**
     * @return NestedObject|ValueObject|null
     */
    public function getObject(): ValueObject|NestedObject|null
    {
        return $this->object;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return StringEnum
     */
    public function getEnum(): StringEnum
    {
        return $this->enum;
    }
}