<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddlewareTest\TestModel;

enum StringEnum: string
{
    case FOO = 'foo';
}