<?php

namespace Debiturio\HydratorMiddlewareTest;

use Debiturio\HydratorMiddleware\CreateHydratorMiddleware;
use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Debiturio\HydratorMiddlewareTest\TestModel\ClassMethodHydratorTestClass;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CreateHydratorMiddlewareTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param array $endpointConfig
     * @param array $input
     * @param string $method
     * @param string $path
     * @param bool $isCollection
     * @return void
     * @throws \ReflectionException
     */
    public function testProcess(array $endpointConfig, array $input, string $method, string $path, bool $isCollection = false)
    {
        $hydratorResult = array_fill(
            0,
            $isCollection ? count($input) : 1,
            $this->createStub(NestedObject::class)
        );

        $hydrator = $this->createMock(HydratorInterface::class);
        $hydrator->method('hydrate')
            ->with($this->callback(function ($data) use ($isCollection, $input) {
                if (!$isCollection) {
                    $this->assertEquals($input, $data);
                } else {
                    $this->assertTrue(in_array($data, $input));
                }
                return true;
            }))
            ->willReturnOnConsecutiveCalls(...$hydratorResult);

        $handler = $this->createMock(RequestHandlerInterface::class);

        $hydratorFactory = $this->createMock(MutateHydratorFactory::class);
        $hydratorFactory->method('getHydrator')->willReturn($hydrator);

        $request = $this->createMock(ServerRequestInterface::class);

        $uri = $this->createStub(UriInterface::class);
        $uri->method('getPath')->willReturn($path);
        $request->method('getMethod')->willReturn($method);
        $request->method('getUri')->willReturn($uri);

        $request->method('getParsedBody')->willReturn($input);
        $request->expects($this->once())
            ->method('withParsedBody')
            ->with(!$isCollection ? $hydratorResult[0] : $hydratorResult)
            ->willReturn($resultRequest = $this->createStub(ServerRequestInterface::class));

        $handler->expects($this->once())
            ->method('handle')
            ->with($resultRequest);

        $middleware = new CreateHydratorMiddleware($hydratorFactory, $endpointConfig);
        $middleware->process($request, $handler);
    }

    public function dataProvider()
    {
        return [
            [
                [
                    '/foo' => [
                        'post' => NestedObject::class
                    ]
                ],
                [
                    'foo' => 'bar',
                    'baz' => 'boo'
                ],
                'post',
                '/foo'
            ],
            [
                [
                    '/foo' => NestedObject::class
                ],
                [
                    [
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ],
                    [
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ],
                    [
                        'foo' => 'bar',
                        'baz' => 'boo'
                    ]
                ],
                'put',
                '/foo',
                true
            ],
        ];
    }
}
