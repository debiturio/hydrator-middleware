<?php

namespace Debiturio\HydratorMiddlewareTest;

use Debiturio\HydratorMiddleware\PropertyNameTrait;
use PHPUnit\Framework\TestCase;

class PropertyNameTraitTest extends TestCase
{
    /**
     * @var PropertyNameTrait
     */
    private object $trait;
    /**
     * @var string[]
     */
    protected function setUp(): void
    {
        $this->trait = $this->getObjectForTrait(\Debiturio\HydratorMiddleware\PropertyNameTrait::class);
    }

    public function testCamelCaseToUnderScore()
    {
        $this->assertEquals(
            'my_new_name',
            $this->trait->camelCaseToSnakeCase('myNewName', '_')
        );
    }

    public function testUnderScoreToCamelCase()
    {
        $this->assertEquals(
            'myNewName',
            $this->trait->snakeCaseToCamelCase('my_new_name', '_')
        );
    }

    // TODO remove deprecated
    /*public function testGetterNameToPropertyName()
    {
        $this->assertEquals(
            'my_name',
            $this->trait->getterNameToPropertyName('getMyName')
        );

        $this->assertEquals(
            'included_users',
            $this->trait->getterNameToPropertyName('hasIncludedUsers')
        );
    }*/

    public function testGetGetterNameWithoutPrefix()
    {
        $this->assertEquals(
            'MyName',
            $this->trait->getGetterNameWithoutPrefix('getMyName', lcFirst: false)
        );

        $this->assertEquals(
            'myName',
            $this->trait->getGetterNameWithoutPrefix('getMyName', lcFirst: true)
        );
    }
}
