<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\NestedObjectHydratorStrategy;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Debiturio\HydratorMiddlewareTest\TestModel\InheritEntity;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class NestedObjectHydratorStrategyTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param $input
     * @return void
     * @throws \ReflectionException
     */
    public function testHydrate($input)
    {
        $hydratedResult = $this->createMock(self::class);
        $repository = $this->createMock(ObjectRepositoryInterface::class);

        if (is_string($input)) {
            $repository->expects($this->once())->method('getById')->with(self::class, $input)->willReturn($hydratedResult);
        }

        $hydrator = $this->createMock(HydratorInterface::class);

        if (is_array($input)) {

            if (in_array('id', $input)) {
                $repository->expects($this->once())
                    ->method('getById')
                    ->with(self::class, $input['id'])
                    ->willReturn($object = $this->createMock(NestedObject::class));

                $inputWithoutId = $input;
                unset($inputWithoutId['id']);

                $hydrator->expects($this->once())->method('hydrate')
                    ->with($inputWithoutId, $object)
                    ->willReturn($hydratedResult);
            }

            if (!in_array('id', $input)) {
                $hydrator->expects($this->once())->method('hydrate')->with($input)->willReturn($hydratedResult);
            }
        }

        $strategy = new NestedObjectHydratorStrategy($repository, $hydrator, self::class);

        $this->assertEquals($hydratedResult, $strategy->hydrate($input, []));
    }

    public function dataProvider(): array
    {
        return [
            ['id'],
            [['foo' => 'bar']],
            [
                [
                    'id' => 'id',
                    'foo' => 'bar'
                ]
            ]

        ];
    }

    public function testHydrateAndAddId()
    {
        $hydrator = $this->createMock(HydratorInterface::class);
        $repository = $this->createMock(ObjectRepositoryInterface::class);
        $strategy = new NestedObjectHydratorStrategy($repository, $hydrator, InheritEntity::class);
        $hydrator->expects($this->once())->method('hydrate')
            ->with($this->callback(function ($value) {
                $this->assertInstanceOf(UuidInterface::class, Uuid::fromString($value['id']));
                return true;
            }))
            ->willReturn($stub = $this->createStub(InheritEntity::class));

        $result = $strategy->hydrate(['name' => 'bob', 'personalAddressAddition' => 'foo', 'age' => 20], []);

        $this->assertEquals($stub, $result);
    }
}
