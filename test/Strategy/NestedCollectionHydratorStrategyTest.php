<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\NestedCollectionHydratorStrategy;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class NestedCollectionHydratorStrategyTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param array $ids
     * @param array $values
     * @param array $mixedValues
     * @return void
     */
    public function testHydrate(array $ids, array $values, array $mixedValues = [])
    {
        $repository = $this->createMock(ObjectRepositoryInterface::class);
        $hydrator = $this->createMock(HydratorInterface::class);
        $strategy = new NestedCollectionHydratorStrategy($repository, $hydrator, NestedObject::class);

        $repository->expects($this->exactly(count($ids) + count($mixedValues)))
            ->method('getById')
            ->withConsecutive(...array_map(function (string|array $item) {
                return [
                    NestedObject::class,
                    is_string($item) ? $item : $item['id']
                ];
            }, [...$ids, ...$mixedValues]))
            ->willReturn($this->createMock(NestedObject::class));

        $hydratorInput = [...$mixedValues, ...$values];

        $hydrator->expects($this->exactly(count($hydratorInput)))
            ->method('hydrate')
            ->withConsecutive(...array_map(function ($item) {

                if (array_key_exists('id', $item)) unset($item['id']);

                return [
                    $this->callback(function ($val) use ($item) {
                        if (array_key_exists('id', $val)) {
                            $this->assertInstanceOf(UuidInterface::class, $val['id']);
                            $this->assertEquals(['id' => $val['id'], ...$item], $val);
                        } else {
                            $this->assertEquals($item, $val);
                        }
                        return true;
                    }),
                    $this->callback(function ($val) {
                        $this->assertInstanceOf(NestedObject::class, $val);
                        return true;
                    })
                ];

            }, $hydratorInput))
            ->willReturn($this->createMock(NestedObject::class));

        $result = $strategy->hydrate($input = [...$ids, ...$values, ...$mixedValues], []);

        $this->assertCount(count($input), $result);
    }

    public function dataProvider(): array
    {
        return [
            [
                [
                    Uuid::uuid4()->toString(),
                    Uuid::uuid4()->toString(),
                    Uuid::uuid4()->toString(),
                ],
                [
                    ['foo' => 'bar'],
                    ['baz' => 'ape']
                ]
            ],
            [
                [
                    Uuid::uuid4()->toString(),
                    Uuid::uuid4()->toString()
                ],
                []
            ],
            [
                [],
                [
                    ['foo' => 'bar'],
                    ['baz' => 'ape']
                ]
            ],
            [
                [
                    Uuid::uuid4()->toString(),
                    Uuid::uuid4()->toString(),
                ],
                [
                    ['foo' => 'bar'],
                    ['baz' => 'ape']
                ],
                [
                    [
                        'id' => Uuid::uuid4()->toString(),
                        'foo' => 'bar'
                    ]
                ]
            ],
        ];
    }
}
