<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\EncryptionStrategy;
use Laminas\Crypt\BlockCipher;
use PHPUnit\Framework\TestCase;

class EncryptionStrategyTest extends TestCase
{

    public function testHydrate()
    {
        $blockCipher = $this->createMock(BlockCipher::class);
        $blockCipher->expects($this->once())->method('encrypt')->with('bob')->willReturn('joe');

        $strategy = new EncryptionStrategy($blockCipher);

        $this->assertEquals('joe', $strategy->hydrate('bob')->getEncrypted());
    }
}
