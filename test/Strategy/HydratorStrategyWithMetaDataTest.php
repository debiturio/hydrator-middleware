<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\HydratorStrategyWithMetaData;
use Laminas\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;

class HydratorStrategyWithMetaDataTest extends TestCase
{
    public function testGetObjectClassName()
    {
        $strategy = new HydratorStrategyWithMetaData(
            $this->createMock(HydratorInterface::class),
            self::class
        );

        $this->assertEquals(self::class, $strategy->getObjectClassName());
    }
}
