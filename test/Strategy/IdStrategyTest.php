<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\IdStrategy;
use Debiturio\HydratorMiddlewareTest\TestModel\NestedObject;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class IdStrategyTest extends TestCase
{
    private IdStrategy $strategy;

    protected function setUp(): void
    {
        $this->strategy = new IdStrategy();
    }

    public function testHydrate()
    {
        $input = Uuid::uuid4()->toString();
        $result = $this->strategy->hydrate($input, []);
        $this->assertInstanceOf(UuidInterface::class, $result);
        $this->assertEquals($input, $result->toString());
    }

    public function testExtract()
    {
        $input = Uuid::uuid4();
        $result = $this->strategy->extract($input);
        $this->assertEquals($input->toString(), $result);

        $input = new NestedObject(Uuid::uuid4(), 'bar', 'foo');
        $result = $this->strategy->extract($input);
        $this->assertEquals($input->getId()->toString(), $result);
    }
}
