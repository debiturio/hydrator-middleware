<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\BackedEnumStrategy;
use Debiturio\HydratorMiddlewareTest\TestModel\StringEnum;
use PHPUnit\Framework\TestCase;

class BackedEnumStrategyTest extends TestCase
{

    public function testHydrate()
    {
        $strategy = new BackedEnumStrategy(StringEnum::class);
        $value = 'foo';
        $this->assertEquals(StringEnum::FOO, $strategy->hydrate($value, []));
    }

    public function testExtract()
    {
        $strategy = new BackedEnumStrategy('');
        $enum = StringEnum::FOO;
        $this->assertEquals('foo', $strategy->extract($enum));
    }
}
