<?php

namespace Debiturio\HydratorMiddlewareTest\Strategy;

use Debiturio\HydratorMiddleware\Strategy\HydratorStrategyWithMetaData;
use Debiturio\HydratorMiddleware\Strategy\UnionTypeHydratorStrategy;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UnionTypeHydratorStrategyTest extends TestCase
{
    public function testExtract()
    {
        $strategyOne = $this->createMock(HydratorStrategyWithMetaData::class);
        $strategyOne->method('getObjectClassName')->willReturn(UuidInterface::class);

        $strategyTwo = $this->createMock(HydratorStrategyWithMetaData::class);
        $strategyTwo->method('getObjectClassName')->willReturn(self::class);

        $valueOne = Uuid::uuid4();
        $valueTwo = $this->createMock(self::class);

        $strategyOne->expects($this->once())->method('extract')->with($valueOne)->willReturn($valueOne->toString());
        $strategyTwo->expects($this->once())->method('extract')->with($valueTwo)->willReturn(['foo' => 'bar']);

        $strategy = new UnionTypeHydratorStrategy([$strategyOne, $strategyTwo]);

        $this->assertEquals($valueOne->toString(), $strategy->extract($valueOne));
        $this->assertEquals(['foo' => 'bar'], $strategy->extract($valueTwo));
    }
}
