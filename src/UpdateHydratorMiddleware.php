<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

use Debiturio\HydratorMiddleware\Factory\BaseHydratorFactoryInterface;
use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Laminas\Diactoros\Response\EmptyResponse;
use Laminas\Hydrator\ClassMethodsHydrator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class UpdateHydratorMiddleware extends AbstractHydrateMiddleware
{
    /**
     * @param ObjectRepositoryInterface $objectRepository
     * @param MutateHydratorFactory $hydratorFactory
     * @param array $endpointsToClasses
     * eg. [ '/foo' => [ 'post' => 'ClassName' ] ] or [ 'bar' => 'ClassName' ]
     */
    public function __construct(private readonly ObjectRepositoryInterface $objectRepository,
                                MutateHydratorFactory $hydratorFactory,
                                private readonly BaseHydratorFactoryInterface $baseHydratorFactory,
                                array $endpointsToClasses)
    {
        parent::__construct($hydratorFactory, $endpointsToClasses);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $data = $request->getParsedBody();

        $objectClassName = $this->getClassName($request);

        $hydrator = $this->hydratorFactory->getHydrator(
            $objectClassName,
            $data,
            $this->baseHydratorFactory->getHydratorFromRequest($request, ClassMethodsHydrator::class)
        );

        if ($this->dataIsCollection($data)) {

            $hydrated = array_map(
                function (array $item, int $index) use ($hydrator, $objectClassName) {

                    $id = array_key_exists('id', $item) ? $item['id'] : null;

                    if (!$id) throw new \Exception(sprintf('Missing id key for entry %s', $index));

                    $object = $this->objectRepository->getById($objectClassName, $id);

                    if (!$object) return new EmptyResponse(404);

                    return $hydrator->hydrate($item, $object);
                },
                $data,
                array_keys($data)
            );

            return $handler->handle($request->withParsedBody($hydrated));
        }

        $object = $this->objectRepository->getById($objectClassName, $request->getAttribute('id'));

        if (!$object) return new EmptyResponse(404);

        return $handler->handle(
            $request->withParsedBody($hydrator->hydrate($data, $object))
        );

    }
}