<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

enum CaseStyle
{
    case CAMEL_CASE;
    case SNAKE_CASE;
}