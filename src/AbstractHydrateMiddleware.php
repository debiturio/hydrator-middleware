<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;

abstract class AbstractHydrateMiddleware implements MiddlewareInterface
{
    public function __construct(protected MutateHydratorFactory $hydratorFactory, private array $endpointsToClasses)
    {
    }

    public function getClassName(ServerRequestInterface $request): string
    {
        /** @var RouteResult $routeResult */
        $routeResult = $request->getAttribute(RouteResult::class);

        $endpoint = $routeResult && array_key_exists($routeResult->getMatchedRouteName(), $this->endpointsToClasses) ?
            $this->endpointsToClasses[$routeResult->getMatchedRouteName()] : null;

        $className = is_array($endpoint) && array_key_exists(strtolower($request->getMethod()), $endpoint) ?
            $endpoint[$request->getMethod()] : null;

        if (!$className && is_string($endpoint)) return $endpoint;

        if (!$className) {
            throw new \Exception(
                sprintf(
                    'Missing object class definition for %s %s',
                    $request->getMethod(),
                    $request->getUri()->getPath()
                )
            );
        }

        return $className;
    }

    public function dataIsCollection(array $data): bool
    {
        return count($data) === count(array_filter(
                array_keys($data),
                function (string|int $key) {
                    return is_int($key);
                }));
    }
}