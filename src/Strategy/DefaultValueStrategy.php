<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;

class DefaultValueStrategy implements StrategyInterface
{
    public function __construct(private readonly mixed $defaultValue)
    {
    }

    public function extract($value, ?object $object = null)
    {

    }

    public function hydrate($value, ?array $data)
    {
        return $this->defaultValue;
    }
}