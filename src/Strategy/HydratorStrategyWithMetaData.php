<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\HydratorInterface;
use Laminas\Hydrator\Strategy\HydratorStrategy;

/**
 * Extension of HydratorStrategy to make the object class name accessible from outside
 */
class HydratorStrategyWithMetaData extends HydratorStrategy
{
    public function __construct(HydratorInterface $objectHydrator, private string $objectClassName)
    {
        parent::__construct($objectHydrator, $objectClassName);
    }

    /**
     * @return string
     */
    public function getObjectClassName(): string
    {
        return $this->objectClassName;
    }

    public function extract($value, ?object $object = null)
    {
        if ($value === null) {
            return null;
        }

        return parent::extract($value, $object);
    }
}