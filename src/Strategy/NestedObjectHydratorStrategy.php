<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Debiturio\HydratorMiddleware\Exception\ReferencedObjectNotFoundException;
use Laminas\Hydrator\HydratorInterface;
use Laminas\Hydrator\Strategy\HydratorStrategy;
use Ramsey\Uuid\Uuid;

class NestedObjectHydratorStrategy extends HydratorStrategy
{
    public function __construct(private readonly ObjectRepositoryInterface $objectRepository,
                                private readonly HydratorInterface         $objectHydrator,
                                private readonly string                    $objectClassName,
                                private readonly string                    $propertyName)
    {
        parent::__construct($objectHydrator, $objectClassName);
    }

    public function extract($value, ?object $object = null)
    {
        if ($value === null) {
            return null;
        }

        return parent::extract($value, $object);
    }

    public function hydrate($value, ?array $data = null)
    {
        if (empty($value)) {
            return null;
        }

        if (is_string($value)) {
            $object = $this->objectRepository->getById($this->objectClassName, $value);
            if (!$object) throw new ReferencedObjectNotFoundException($value, $this->propertyName);
            return $object;
        }

        if (is_array($value) && array_key_exists('id', $value) && is_string($value['id'])) {
            $object = $this->objectRepository->getById($this->objectClassName, $value['id']);
            if (!$object) throw new ReferencedObjectNotFoundException($value['id'], $this->propertyName);
            unset($value['id']);
            return $this->objectHydrator->hydrate($value, $object);
        }

        if(is_array($value) && !array_key_exists('id', $value) && property_exists($this->objectClassName, 'id')) {
            $value['id'] = Uuid::uuid4()->toString();
        }

        return parent::hydrate($value, $data);
    }
}