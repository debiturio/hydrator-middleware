<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Crypt\BlockCipher;
use Laminas\Hydrator\Strategy\StrategyInterface;

class EncryptionStrategy implements StrategyInterface
{
    public function __construct(private readonly BlockCipher $blockCipher)
    {
    }

    public function hydrate($value, ?array $data = null)
    {
        if (!is_scalar($value)) {
            throw new \InvalidArgumentException(sprintf('Value needs to be a string, int, float or bool - %s given', gettype($value)));
        }

        return Encrypted::fromPlainText((string) $value, $this->blockCipher);
    }

    public function extract($value, ?object $object = null)
    {
        if (!$value instanceof Encrypted) {
            throw new \InvalidArgumentException(sprintf('Value needs to be a %s - %s given', Encrypted::class, gettype($value)));
        }

        return '';
    }
}