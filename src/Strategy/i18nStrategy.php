<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;
use Laminas\I18n\Translator\TranslatorInterface;

class i18nStrategy implements StrategyInterface
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function extract($value, ?object $object = null)
    {
        if ($value instanceof i18nTranslatableValue) {
            return $this->translator->translate($value->getValue());
        }
    }

    public function hydrate($value, ?array $data)
    {

    }
}