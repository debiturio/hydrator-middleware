<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;

class BackedEnumStrategy implements StrategyInterface
{
    public function __construct(private readonly string $enumClassName)
    {
    }

    /**
     * @param \BackedEnum $value
     * @param object|null $object
     * @return mixed
     */
    public function extract($value, ?object $object = null)
    {
        return $value ? $value->value : null;
    }

    /**
     * @param $value
     * @param array|null $data
     * @return mixed|void
     */
    public function hydrate($value, ?array $data)
    {
        return $this->enumClassName::tryFrom($value);
    }
}