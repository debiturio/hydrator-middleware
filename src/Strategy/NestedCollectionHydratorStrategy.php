<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Debiturio\HydratorMiddleware\Exception\ReferencedObjectNotFoundException;
use Laminas\Hydrator\HydratorInterface;
use Laminas\Hydrator\Strategy\CollectionStrategy;
use Ramsey\Uuid\Uuid;

class NestedCollectionHydratorStrategy extends CollectionStrategy
{
    /**
     * @param ObjectRepositoryInterface $objectRepository
     * @param HydratorInterface $objectHydrator
     * @param string $objectClassName
     * @param string $propertyName
     * @param string|null $collectionType
     */
    public function __construct(
        private readonly ObjectRepositoryInterface $objectRepository,
        private readonly HydratorInterface         $objectHydrator,
        private readonly string                    $objectClassName,
        private readonly string                    $propertyName,
        private readonly ?string                   $collectionType = null)
    {
        parent::__construct($this->objectHydrator, $objectClassName);
    }

    /**
     * Hydrates all items.
     * If an item includes an id the object gets fetched from the object repository.
     * If the item includes more values, they get applied.
     *
     * @param $value
     * @param array|null $data
     * @return array|mixed|object[]
     * @throws ReferencedObjectNotFoundException
     */
    public function hydrate($value, ?array $data = null)
    {
        $idValues = array_filter($value, function (mixed $item) {
            return is_string($item) && Uuid::isValid($item);
        });

        $valuesToUpdate = array_filter($value, function (mixed $item) {
            return is_array($item) && array_key_exists('id', $item) && is_string($item['id']);
        });

        $simpleFetchedObjects = array_map(
            function (string $item) {
                $object = $this->objectRepository->getById($this->objectClassName, $item);
                if (!$object) throw new ReferencedObjectNotFoundException($item, $this->propertyName);
                return $object;
            },
            $idValues
        );

        $valuesToCreate = array_map(
            function ($item) {
                return [
                    ...$item,
                    'id' => Uuid::uuid4()
                ];
            },
            array_filter($value, function ($item) use ($valuesToUpdate, $idValues) {
                return !is_string($item) && !array_key_exists('id', $item);
            })
        );

        $updatedValues = array_map(
            function (array $item) {
                $object = $this->objectRepository->getById($this->objectClassName, $item['id']);
                if (!$object) throw new ReferencedObjectNotFoundException($item['id'], $this->propertyName);
                $data = $item;
                unset($data['id']);
                return $this->objectHydrator->hydrate($data, $object);
            },
            $valuesToUpdate
        );

        $hydratedItems = [
            ...parent::hydrate($valuesToCreate, $data),
            ...$updatedValues,
            ...$simpleFetchedObjects
        ];

        if (!$this->collectionType || strtolower($this->collectionType) === 'array') return $hydratedItems;

        $cl = $this->collectionType;

        return is_subclass_of($cl, CollectionInterface::class) ? $cl::createFromArray($hydratedItems) : new $cl($hydratedItems);
    }
}