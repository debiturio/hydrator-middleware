<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

class CurrencyAmount
{
    public function __construct(private readonly float $amount)
    {
    }

    public static function fromFloat(float $amount): self
    {
        return new self($amount);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}