<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Crypt\BlockCipher;

class Encrypted
{
    public function __construct(private readonly string $encrypted)
    {
    }

    public static function fromPlainText(string $plainText, BlockCipher $blockCipher): self
    {
        return new self($blockCipher->encrypt($plainText));
    }

    public function getDecrypted(BlockCipher $blockCipher): string|bool
    {
        return $blockCipher->decrypt($this->encrypted);
    }

    /**
     * @return string
     */
    public function getEncrypted(): string
    {
        return $this->encrypted;
    }
}