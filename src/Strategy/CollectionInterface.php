<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

interface CollectionInterface
{
    public static function createFromArray(array $array): self;
}