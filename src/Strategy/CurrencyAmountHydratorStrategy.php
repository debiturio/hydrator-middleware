<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;

class CurrencyAmountHydratorStrategy implements StrategyInterface
{

    public function __construct(private readonly ?\NumberFormatter $currencyFormatter = null)
    {
    }

    /**
     * @param CurrencyAmount $value
     * @param object|null $object
     * @return mixed
     */
    public function extract($value, ?object $object = null)
    {
        if (!$value) {
            return $value;
        }

        return $this->currencyFormatter ? $this->currencyFormatter->format($value->getAmount()) : $value->getAmount();
    }

    public function hydrate($value, ?array $data)
    {
        if (!$value) {
            return null;
        }

        return new CurrencyAmount($value);
    }
}