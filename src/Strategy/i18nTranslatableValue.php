<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

class i18nTranslatableValue
{
    public function __construct(private readonly string $value)
    {
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}