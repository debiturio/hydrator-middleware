<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Strategy\StrategyInterface;
use Ramsey\Uuid\Nonstandard\Uuid;
use Ramsey\Uuid\UuidInterface;

class IdStrategy implements StrategyInterface
{
    public function extract($value, ?object $object = null)
    {
        if (is_iterable($value)) {
            return array_filter(array_map(function ($item) {
                return !is_iterable($item) ? $this->extract($item) : null;
            }, is_array($value) ? $value : iterator_to_array($value)));
        }

        if ($value instanceOf UuidInterface) return (string) $value;

        return is_object($value) && method_exists($value, 'getId') ? (string) $value->getId() : null;
    }

    public function hydrate($value, ?array $data)
    {
        if ($value instanceof UuidInterface) {
            return $value;
        }

        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Value needs to be a string - %s given', gettype($value)));
        }

        return Uuid::fromString($value);
    }
}