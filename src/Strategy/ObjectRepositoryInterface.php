<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Ramsey\Uuid\UuidInterface;

interface ObjectRepositoryInterface
{
    public function getById(string $objectClassName, string|UuidInterface $id): ?object;
}