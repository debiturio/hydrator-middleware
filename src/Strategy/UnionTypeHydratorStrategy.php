<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

use Laminas\Hydrator\Exception\InvalidArgumentException;
use Laminas\Hydrator\Strategy\HydratorStrategy;
use Laminas\Hydrator\Strategy\StrategyInterface;

class UnionTypeHydratorStrategy implements StrategyInterface
{
    /**
     * @param HydratorStrategyWithMetaData[] $hydratorStrategies
     */
    public function __construct(private array $hydratorStrategies)
    {
    }

    public function extract($value, ?object $object = null)
    {
        if (is_scalar($value) || is_null($value)) return $value;

        if (is_iterable($value)) {

            if (is_array($value) && count(array_filter($value, 'is_scalar')) === count($value)) {
                return $value;
            }

            return array_map(function ($item) use ($object) {
                return $this->extract($item, $object);
            }, is_array($value) ? $value : iterator_to_array($value));

        }

        return $this->getMatchingExtractorStrategy($value)->extract($value, $object);
    }

    public function hydrate($value, ?array $data)
    {
        // Not supported yet
    }

    private function getMatchingExtractorStrategy(object $value): HydratorStrategy
    {
        $matchingHydratorStrategies = array_filter($this->hydratorStrategies, function (HydratorStrategyWithMetaData $strategy) use ($value) {
            $className = $strategy->getObjectClassName();
            return $value instanceof $className;
        });

        uksort($matchingHydratorStrategies, function ($a, $b) use ($value) {
            $aIsClass = get_class($value) === $a;
            $bIsClass = get_class($value) === $b;

            if ($aIsClass === $bIsClass) return 0;

            return $aIsClass ? 1 : -1;

        });

        $matchingHydratorStrategies = array_values($matchingHydratorStrategies);

        if (empty($matchingHydratorStrategies)) {
            throw new InvalidArgumentException(sprintf('No strategy defined for type %s', get_class($value)));
        }

        return $matchingHydratorStrategies[0];
    }
}