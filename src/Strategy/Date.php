<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Strategy;

class Date extends \DateTimeImmutable
{
    public static function fromDateTime(\DateTimeInterface $dateTime): self
    {
        return new self($dateTime->format(self::ATOM));
    }
}