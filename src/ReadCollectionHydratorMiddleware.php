<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

use Debiturio\HydratorMiddleware\Factory\ReadHydratorFactory;
use Debiturio\HydratorMiddleware\Property\PropertiesHydrator;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\Persistence\Proxy;
use Laminas\Hydrator\Strategy\CollectionStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadCollectionHydratorMiddleware implements MiddlewareInterface
{
    /**
     * @param ReadHydratorFactory $hydratorFactory
     * @param PropertiesHydrator $propertiesHydrator
     * @param CaseStyle $caseStyle
     * @param string $dataKey
     */
    public function __construct(
        private ReadHydratorFactory $hydratorFactory,
        private PropertiesHydrator $propertiesHydrator,
        private CaseStyle $caseStyle = CaseStyle::CAMEL_CASE,
        private string $dataKey = 'data')
    {
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \ReflectionException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $params = $request->getQueryParams();
        $propertyData = array_key_exists('properties', $params) ? $params['properties'] : [];

        $collectionProperties = $this->propertiesHydrator->hydrate(
            is_string($propertyData) ? explode(',', $propertyData) : $propertyData,
        );

        $body = $request->getParsedBody();
        $data = is_array($body) && array_key_exists($this->dataKey, $body) ? $body[$this->dataKey] : $body;
        $isCollection = is_iterable($data);
        $firstElement = is_object($data) ? $data : null;

        if ($isCollection && count($data) > 0) {

            // TODO fix if first element is proxy
            /*$elementsClone = clone $data;

            foreach ($elementsClone as $item) {
                $firstElement = $item;
                if (!$firstElement instanceof Proxy) {
                    break;
                }
            }*/

            if (is_array($data)) {
                $firstElement = $data[array_key_first($data)];
            } elseif ($data instanceof \Iterator) {
                $firstElement = $data->current();
            }
        }

        $objectClassName = $firstElement ? ClassUtils::getRealClass(get_class($firstElement)) : null;

        $hydrator = $objectClassName ?
            $this->hydratorFactory->getHydrator($objectClassName, $collectionProperties, $this->caseStyle) : null;
        $hydrator = $isCollection && $hydrator ? new CollectionStrategy($hydrator, $objectClassName) : $hydrator;

        $request = $hydrator ? $request->withParsedBody([...$body, $this->dataKey => $hydrator->extract($data)]) : $request;

        return $handler->handle($request);
    }
}