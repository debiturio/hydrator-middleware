<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Property;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\PropertyNameTrait;
use Laminas\Hydrator\Filter\FilterInterface;

class PropertyFilter implements FilterInterface
{
    use PropertyNameTrait;

    private array $methods;

    private array $excludedMethods;

    private bool $allGettersAllowed;

    /**
     * @param CaseStyle $caseStyle
     * @param PropertyCollection|null $propertyCollection
     * @param PropertyCollection|null $excludedProperties
     */
    public function __construct(
        private CaseStyle $caseStyle,
        PropertyCollection $propertyCollection = null,
        PropertyCollection $excludedProperties = null
    ) {
        $this->allGettersAllowed = !$propertyCollection || !!$propertyCollection?->getPropertyByKey('*');
        $this->excludedMethods = $excludedProperties ? $this->propertiesToMethods($excludedProperties) : [];
        $this->methods = !$this->allGettersAllowed ? $this->propertiesToMethods($propertyCollection) : [];
    }

    /**
     * @param PropertyCollection $properties
     * @return array
     */
    private function propertiesToMethods(PropertyCollection $properties): array
    {
        return array_map(
            function (Property $property) {
                return sprintf(
                    'get%s',
                    ucfirst(
                        $this->convertCaseStyle(
                            $property->getKey(),
                            $this->caseStyle,
                            CaseStyle::CAMEL_CASE
                        )
                    )
                );
            },
            $properties->getArrayCopy()
        );
    }

    /**
     * @param string $property
     * @param object|null $instance
     * @return bool
     */
    public function filter(string $property, ?object $instance = null): bool
    {
        $pos = strpos($property, '::');
        if ($pos !== false) {
            $pos += 2;
        } else {
            $pos = 0;
        }

        $method = substr($property, $pos);

        if (in_array($method, $this->excludedMethods)) return false;

        if ($this->allGettersAllowed && str_starts_with($method, 'get') &&
            ctype_upper(substr($method, 3, 1))) {
            return true;
        };

        return in_array($method, $this->methods);
    }
}