<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Property;

class ExcludedPropertyCollection
{
    /**
     * @param PropertyCollection $default
     * @param PropertyCollection[] $excluded
     */
    public function __construct(private PropertyCollection $default, private array $excluded = [])
    {
    }

    /**
     * @param array $data
     * @return PropertyCollection[]
     */
    public static function fromArray(array $data): self
    {
        $hydrator = new PropertiesHydrator();

        $defaultExcluded = array_filter(
            $data,
            function (array|string $item) {
                return is_string($item);
            }
        );

        $classSpecificExcluded = array_filter(
            $data,
            function (array|string $item) {
                return is_array($item);
            }
        );

        $default = !empty($defaultExcluded) ? $hydrator->hydrate($defaultExcluded) : new PropertyCollection();

        $result = [];

        foreach ($classSpecificExcluded as $className => $items)
            $result[$className] = $hydrator->hydrate([...$defaultExcluded, ...$items]);

        return new self($default, $result);

    }

    /**
     * @return PropertyCollection
     */
    public function getDefault(): PropertyCollection
    {
        return $this->default;
    }

    public function getByKey(string $key): PropertyCollection
    {
        return array_key_exists($key, $this->excluded) ? $this->excluded[$key] : $this->default;
    }
}