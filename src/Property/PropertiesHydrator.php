<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Property;

class PropertiesHydrator
{
    /**
     * @param PropertyCollection $properties
     * @return array
     */
    public function extract(PropertyCollection $properties): array
    {
        $result = [];

        $asterix = $properties->getPropertyByKey('*');
        $asterixPath = $asterix?->getNestedProperties() ? $this->extract($asterix->getNestedProperties())[0] : null;

        foreach ($properties as $property) {

            if (!$property->hasNestedProperties()) {
                $result[] = $property->getKey();
                continue;
            }

            foreach ($this->extract($property->getNestedProperties()) as $value) {

                if ($asterixPath && $asterix !== $property && substr_count($value, '.') === substr_count($asterixPath, '.')) {
                    continue;
                }

                $result[] = sprintf('%s.%s', $property->getKey(), $value);
            }

        }

        return $result;
    }

    /**
     * @param array $data
     * @return PropertyCollection
     */
    public function hydrate(array $data): PropertyCollection
    {
        if (empty($data)) {
            $data = ['*'];
        }

        $grouped = [];

        $asterixPaths = array_filter($data, function ($item) {
            return str_starts_with($item, '*') && strlen(str_replace('.', '', $item)) > 1;
        });

        usort($asterixPaths, function (string $a, string $b) {
            return strlen($b) - strlen($a);
        });

        $asterixPath = !empty($asterixPaths) ? substr($asterixPaths[0], 2) : null;

        foreach ($data as $item) {

            $keys = explode('.', trim($item));
            $key = trim($keys[0]);

            if ($asterixPath && !array_key_exists($key, $grouped)) $grouped[$key] = [$asterixPath];

            if (count($keys) > 1 && $key !== '*') {
                $grouped[$key][] = implode('.', array_slice($keys, 1));
            }

            if(!$asterixPath && count($keys) === 1 && !array_key_exists($key, $grouped)) $grouped[$key] = null;

        }

        return new PropertyCollection(...array_map(
            function ($items, $key) {
                return new Property($key, !empty($items) ? $this->hydrate($items) : null);
            },
            $grouped,
            array_keys($grouped)
        ));
    }
}