<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Property;

class PropertyCollection extends \ArrayIterator
{
    /**
     * @param Property ...$properties
     */
    public function __construct(Property ...$properties)
    {
        parent::__construct($properties);
    }

    /**
     * @return Property
     */
    public function current(): Property
    {
        return parent::current();
    }

    /**
     * @param string $key
     * @return Property|null
     */
    public function getPropertyByKey(string $key): ?Property
    {
        /** @var Property $item */
        foreach ($this->getArrayCopy() as $item) {
            if ($item->getKey() === $key) return $item;
        }

        return null;
    }

    public function isEmpty(): bool
    {
        return $this->count() === 0;
    }
}