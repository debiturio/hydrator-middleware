<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Property;

class Property
{
    /**
     * @param string $key
     * @param PropertyCollection|null $nestedProperties
     */
    public function __construct(private string $key, private ?PropertyCollection $nestedProperties = null)
    {
    }

    /**
     * @return PropertyCollection|null
     */
    public function getNestedProperties(): ?PropertyCollection
    {
        return $this->nestedProperties;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    public function hasNestedProperties(): bool
    {
        return $this->nestedProperties && $this->nestedProperties->count() > 0;
    }
}