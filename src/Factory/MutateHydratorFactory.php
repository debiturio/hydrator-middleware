<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\PropertyNameTrait;
use Debiturio\HydratorMiddleware\Strategy\BackedEnumStrategy;
use Debiturio\HydratorMiddleware\Strategy\DefaultValueStrategy;
use Debiturio\HydratorMiddleware\Strategy\Encrypted;
use Debiturio\HydratorMiddleware\Strategy\EncryptionStrategy;
use Debiturio\HydratorMiddleware\Strategy\IdStrategy;
use Debiturio\HydratorMiddleware\Strategy\NestedCollectionHydratorStrategy;
use Debiturio\HydratorMiddleware\Strategy\NestedObjectHydratorStrategy;
use Debiturio\HydratorMiddleware\Strategy\ObjectRepositoryInterface;
use Laminas\Crypt\BlockCipher;
use Laminas\Hydrator\HydratorInterface;
use Laminas\Hydrator\ReflectionHydrator;
use Laminas\Hydrator\Strategy\DateTimeFormatterStrategy;
use Laminas\Hydrator\Strategy\DateTimeImmutableFormatterStrategy;
use Laminas\Hydrator\Strategy\StrategyInterface;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\UuidInterface;

class MutateHydratorFactory extends AbstractHydratorFactory implements MutateHydratorFactoryInterface
{
    use PropertyNameTrait;

    public function __construct(
        ContainerInterface  $container,
        private readonly ObjectRepositoryInterface $objectRepository,
        ExcludedPropertyCollection $excludedProperties,
        array $collectionItemTypes,
        array $strategyConfiguration,
        ?BlockCipher $blockCipher,
        private readonly CaseStyle $caseStyle = CaseStyle::CAMEL_CASE)
    {
        parent::__construct($container, $excludedProperties, $collectionItemTypes, $strategyConfiguration, [], $blockCipher);
    }

    /**
     * @param string $objectClassName
     * @return HydratorInterface
     * @throws \ReflectionException
     */
    public function getHydrator(string $objectClassName, mixed $data, HydratorInterface $hydrator = null): HydratorInterface
    {
        $hydrator = $hydrator ?: new ReflectionHydrator();
        $emptyHydrator = clone $hydrator;

        $reflection = new \ReflectionClass($objectClassName);

        $properties = $reflection->getProperties();

        if ($reflection->hasProperty('id')) {
            $hydrator->addStrategy(
                'id',
                new IdStrategy()
            );
        }

        $excludedProperties = $this->excludedProperties->getByKey($objectClassName);

        foreach ($properties as $property) {

            $filteredPropName = $this->filterPropertyName($property->getName(), $this->caseStyle);
            if ($excludedProperties->getPropertyByKey($property->name)) continue;

            if (!is_array($data)) continue;

            if (!array_key_exists($filteredPropName, $data)) {

                if ($property->hasDefaultValue()) {

                    $hydrator->addStrategy(
                        $filteredPropName,
                        new DefaultValueStrategy($property->getDefaultValue())
                    );

                }

                continue;
            }


            // Warning: We don't support union types atm
            if ($property->getType() instanceof \ReflectionNamedType) {

                $strategy = $this->getStrategy(
                    $property->getType(),
                    $objectClassName,
                    $property->getName(),
                    is_array($data) ? $data[$filteredPropName] : $data,
                    clone $emptyHydrator
                );

                if ($strategy) {
                    $hydrator->addStrategy($filteredPropName, $strategy);
                }

            }

            if ($property->getType() instanceof \ReflectionUnionType) {

                $firstType = $property->getType()->getTypes()[0];

                $strategy = $this->getStrategy(
                    $firstType,
                    $objectClassName,
                    $property->getName(),
                    is_array($data) ? $data[$filteredPropName] : $data,
                    clone $emptyHydrator
                );

                if ($strategy) {
                    $hydrator->addStrategy($filteredPropName, $strategy);
                }

            }

        }

        return $hydrator;
    }

    /**
     * @param \ReflectionNamedType $type
     * @param string $objectClassName
     * @param string $propertyName
     * @param mixed $data
     * @param HydratorInterface $emptyHydrator
     * @return StrategyInterface|null
     * @throws \ReflectionException
     */
    private function getStrategy(\ReflectionNamedType $type, string $objectClassName, string $propertyName, mixed $data, HydratorInterface $emptyHydrator): ?StrategyInterface
    {
        $typeClass = !$type->isBuiltin() ? new \ReflectionClass($type->getName()) : null;

        $confStrategy = $this->getConfiguredStrategy($type);
        if ($confStrategy) return $confStrategy;

        if ($this->blockCipher && $type->getName() === Encrypted::class) {
            return new EncryptionStrategy($this->blockCipher);
        }

        if ($type->getName() === \DateTime::class) {
            return new DateTimeFormatterStrategy('c', null, true);
        }

        if ($type->getName() === \DateTimeImmutable::class) {
            return new DateTimeImmutableFormatterStrategy(
                new DateTimeFormatterStrategy('c', null, true)
            );
        }

        if ($typeClass && $typeClass->implementsInterface(\UnitEnum::class)) {
            if ($typeClass->implementsInterface(\BackedEnum::class)) {
                return new BackedEnumStrategy($typeClass->getName());
            }
        }

        if ($typeClass &&
            !$typeClass->implementsInterface(\UnitEnum::class) &&
            !$typeClass->isInternal() &&
            !$typeClass->implementsInterface(\Traversable::class) &&
            !$typeClass->implementsInterface(UuidInterface::class)
        ) {
            return new NestedObjectHydratorStrategy(
                $this->objectRepository,
                $this->getHydrator($type->getName(), $data, $emptyHydrator),
                $type->getName(),
                $propertyName
            );
        }

        if (strtolower($type->getName()) === 'array' ||
            ($typeClass && $typeClass->implementsInterface(\Traversable::class))) {

            $collectionItems = $this->getCollectionItemClassNames($objectClassName, $propertyName);

            $depth = array_map(function ($item) {
                return is_array($item) ? $this->arrayDepth($item) : 0;
            }, $data);

            arsort($depth);

            $keyOfDeepestItem = array_key_first($depth);

            // Warning: We don't support union types atm
            if (count($collectionItems) === 1) {
                return new NestedCollectionHydratorStrategy(
                    $this->objectRepository,
                    $this->getHydrator($collectionItems[0], $data[$keyOfDeepestItem], $emptyHydrator),
                    $collectionItems[0],
                    $propertyName,
                    $type->getName()
                );
            }

        }

        return null;
    }

    /**
     * @param string $name
     * @param CaseStyle $caseStyle
     * @return string
     */
    private function filterPropertyName(string $name, CaseStyle $caseStyle): string
    {
        return $caseStyle !== CaseStyle::CAMEL_CASE ? $this->camelCaseToSnakeCase($name) : $name;
    }

    private function arrayDepth(array $array): int
    {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->arrayDepth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }
}