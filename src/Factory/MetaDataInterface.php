<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

interface MetaDataInterface
{
    public function get(string $key): ?object;
}