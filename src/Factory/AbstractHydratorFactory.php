<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\PropertyNameTrait;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\Persistence\Proxy;
use Laminas\Crypt\BlockCipher;
use Laminas\Hydrator\Strategy\StrategyInterface;
use Psr\Container\ContainerInterface;

abstract class AbstractHydratorFactory
{
    use PropertyNameTrait;

    public function __construct(protected readonly ContainerInterface   $container,
                                protected ExcludedPropertyCollection    $excludedProperties,
                                protected array                         $collectionItemTypes = [],
                                protected array                         $filterConfiguration = [],
                                protected array                         $strategyConfiguration = [],
                                protected readonly ?BlockCipher         $blockCipher = null)
    {
    }

    /**
     * @param string $object
     * @param string $property
     * @return string[]|string
     */
    public function getCollectionItemClassNames(string $object, string $property): array|string
    {
        // TODO wrap this into a filter to remove doctrine dependency
        $object = ClassUtils::getRealClass($object);

        if (!array_key_exists($object, $this->collectionItemTypes)) {
            throw new \InvalidArgumentException(sprintf(
                'Missing collection types for type %s', $object
            ));
        }

        if (!array_key_exists($property, $this->collectionItemTypes[$object])) {
            throw new \InvalidArgumentException(sprintf(
                'Missing collection types for property %s of type %s', $property, $object
            ));
        }

        return $this->collectionItemTypes[$object][$property];
    }

    public function getConfiguredStrategy(\ReflectionNamedType $type): ?StrategyInterface
    {
        return array_key_exists($type->getName(), $this->strategyConfiguration) &&
        $this->container->has($this->strategyConfiguration[$type->getName()]) ?
            $this->container->get($this->strategyConfiguration[$type->getName()]) : null;
    }

    public function getFilters(string $class): array
    {
        $filters = array_key_exists($class, $this->filterConfiguration) ? $this->filterConfiguration[$class] : [];

        return array_filter(array_map(function ($filter) use ($class) {
            return $this->container->has($filter) ?
                $this->container->get($filter) : null;
        }, $filters));
    }
}