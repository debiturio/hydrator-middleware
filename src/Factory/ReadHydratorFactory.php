<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

use Debiturio\HydratorMiddleware\CaseStyle;
use Debiturio\HydratorMiddleware\Property\ExcludedPropertyCollection;
use Debiturio\HydratorMiddleware\Property\Property;
use Debiturio\HydratorMiddleware\Property\PropertyCollection;
use Debiturio\HydratorMiddleware\Property\PropertyFilter;
use Debiturio\HydratorMiddleware\Strategy\BackedEnumStrategy;
use Debiturio\HydratorMiddleware\Strategy\CurrencyAmount;
use Debiturio\HydratorMiddleware\Strategy\CurrencyAmountHydratorStrategy;
use Debiturio\HydratorMiddleware\Strategy\Date;
use Debiturio\HydratorMiddleware\Strategy\Encrypted;
use Debiturio\HydratorMiddleware\Strategy\EncryptionStrategy;
use Debiturio\HydratorMiddleware\Strategy\HydratorStrategyWithMetaData;
use Debiturio\HydratorMiddleware\Strategy\i18nStrategy;
use Debiturio\HydratorMiddleware\Strategy\i18nTranslatableValue;
use Debiturio\HydratorMiddleware\Strategy\IdStrategy;
use Debiturio\HydratorMiddleware\Strategy\Time;
use Debiturio\HydratorMiddleware\Strategy\UnionTypeHydratorStrategy;
use Laminas\Hydrator\ClassMethodsHydrator;
use Laminas\Hydrator\Filter\FilterComposite;
use Laminas\Hydrator\Filter\GetFilter;
use Laminas\Hydrator\HydratorInterface;
use Laminas\Hydrator\Strategy\CollectionStrategy;
use Laminas\Hydrator\Strategy\DateTimeFormatterStrategy;
use Laminas\Hydrator\Strategy\DateTimeImmutableFormatterStrategy;
use Laminas\Hydrator\Strategy\StrategyInterface;
use Laminas\I18n\Translator\TranslatorInterface;
use Ramsey\Uuid\UuidInterface;

class ReadHydratorFactory extends AbstractHydratorFactory
{
    private array $customStrategies = [];

    /**
     * @param string $objectClassName
     * @param PropertyCollection|null $propertyCollection
     * @param CaseStyle $outputStyle
     * @param array $customStrategies
     * @return HydratorInterface
     * @throws \ReflectionException
     */
    public function getHydrator(
        string $objectClassName,
        PropertyCollection $propertyCollection = null,
        CaseStyle $outputStyle = CaseStyle::CAMEL_CASE,
        array $customStrategies = []): HydratorInterface
    {
        if (!empty($customStrategies)) {
            $this->customStrategies = $customStrategies;
        }

        $hydrator = new ClassMethodsHydrator($outputStyle !== CaseStyle::CAMEL_CASE);

        // Clean up hydrator and remove default filter
        $hydrator->removeFilter('get');
        $hydrator->removeFilter('has');
        $hydrator->removeFilter('is');

        $reflection = new \ReflectionClass($objectClassName);

        $excludedProperties = $this->excludedProperties->getByKey($objectClassName);

        $getters = array_filter($reflection->getMethods(), function (\ReflectionMethod $method) {
            $filter = new GetFilter();
            return $filter->filter($method->getName());
        });

        $hydrator->addStrategy('id', new IdStrategy());

        foreach ($getters as $getter) {

            $returnType = $getter->hasReturnType() ? $getter->getReturnType() : null;
            $propertyName = $this->getGetterNameWithoutPrefix($getter->getName());
            $property = $propertyCollection?->getPropertyByKey($propertyName);
            $property = $property ?: new Property($propertyName);

            if ($excludedProperties->getPropertyByKey($propertyName)) {
                continue;
            }

            if ($returnType instanceof \ReflectionNamedType) {
                $strategy = $this->getStrategy($objectClassName, $property, $returnType);
                if ($strategy) $hydrator->addStrategy($propertyName, $strategy);
            }

            if ($returnType instanceof \ReflectionUnionType) {

                $strategies = array_filter(
                    array_map(
                        function (\ReflectionNamedType $type) use ($property, $objectClassName) {
                            return $this->getStrategy($objectClassName, $property, $type);
                        },
                        $returnType->getTypes()
                    )
                );

                if (!empty($strategies)) {

                    $hydrator->addStrategy(
                        $propertyName,
                        $property->hasNestedProperties() ? new UnionTypeHydratorStrategy(
                            $strategies,
                        ) : new IdStrategy()
                    );

                }

            }

        }

        $filters = $this->getFilters($objectClassName);

        foreach ($filters as $key => $filter) {
            $hydrator->addFilter(
                'custom_filter_' . $key,
                $filter,
                FilterComposite::CONDITION_AND
            );
        }

        $hydrator->addFilter(
            'propertiesToInclude',
            new PropertyFilter($outputStyle, $propertyCollection, $excludedProperties),
            FilterComposite::CONDITION_AND
        );

        return $hydrator;
    }

    /**
     * @param string $objectClassName
     * @param Property $property
     * @param \ReflectionNamedType $returnType
     * @return StrategyInterface|null
     * @throws \ReflectionException
     */
    private function getStrategy(
        string $objectClassName,
        Property $property,
        \ReflectionNamedType $returnType): ?StrategyInterface
    {
        $returnTypeClass = !$returnType->isBuiltin() ? new \ReflectionClass($returnType->getName()) : null;

        if ($returnTypeClass && array_key_exists($returnTypeClass->getName(), $this->customStrategies) &&
            $this->customStrategies[$returnTypeClass->getName()] instanceof StrategyInterface) {
            return $this->customStrategies[$returnTypeClass->getName()];
        }

        $confStrategy = $this->getConfiguredStrategy($returnType);
        if ($confStrategy) return $confStrategy;

        if ($this->blockCipher && $returnType->getName() === Encrypted::class) {
            return new EncryptionStrategy($this->blockCipher);
        }

        if ($returnType->getName() === CurrencyAmount::class) {
            return new CurrencyAmountHydratorStrategy();
        }

        if ($returnType->getName() === Time::class) {
            return new DateTimeFormatterStrategy('H:i:s', null, true);
        }

        if ($returnType->getName() === Date::class) {
            return new DateTimeFormatterStrategy('Y-m-d', null, true);
        }

        if (in_array($returnType->getName(), [\DateTime::class, \DateTimeInterface::class, \DateTimeImmutable::class])) {
            return new DateTimeFormatterStrategy(DATE_ATOM, null, true);
        }

        // TODO remove
        if ($returnType->getName() === \DateTimeImmutable::class) {
            return new DateTimeImmutableFormatterStrategy(
                new DateTimeFormatterStrategy(DATE_ATOM, null, true)
            );
        }

        // TODO add to tests
        if ($returnTypeClass && $returnTypeClass->implementsInterface(\UnitEnum::class)) {
            if ($returnTypeClass->implementsInterface(\BackedEnum::class)) {
                return new BackedEnumStrategy($returnTypeClass->getName());
            }
        }

        if ($returnType->getName() === i18nTranslatableValue::class && $this->container->has(TranslatorInterface::class)) {
            return new i18nStrategy($this->container->get(TranslatorInterface::class));
        }

        if ($returnTypeClass &&
            !$returnTypeClass->implementsInterface(\UnitEnum::class) &&
            !$returnTypeClass->isInternal() &&
            !$returnTypeClass->implementsInterface(\Traversable::class) &&
            !$returnTypeClass->implementsInterface(UuidInterface::class)
        ) {
            return $property->hasNestedProperties() ? new HydratorStrategyWithMetaData(
                $this->getHydrator(
                    $returnType->getName(),
                    $property->getNestedProperties()
                ),
                $returnType->getName()
            ) : new IdStrategy();
        }

        if (strtolower($returnType->getName()) === 'array' ||
            ($returnTypeClass && $returnTypeClass->implementsInterface(\Traversable::class))) {

            $collectionItems = $this->getCollectionItemClassNames($objectClassName, $property->getKey());

            if (is_string($collectionItems) && strtolower($collectionItems) === 'plain') {
                return null;
            }

            return $property->hasNestedProperties() ? new UnionTypeHydratorStrategy(
                array_map(
                    function (string $name) use ($property) {
                        return new HydratorStrategyWithMetaData(
                            $this->getHydrator(
                                $name,
                                $property->getNestedProperties()
                            ),
                            $name,
                        );
                    },
                    is_string($collectionItems) ? [$collectionItems] : $collectionItems
                )
            ) : new IdStrategy();
        }

        return null;
    }
}