<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

use Laminas\Hydrator\HydratorInterface;
use Psr\Http\Message\RequestInterface;

interface BaseHydratorFactoryInterface
{
    public function getHydratorFromRequest(RequestInterface $request, string $hydratorClass = null): HydratorInterface;
}