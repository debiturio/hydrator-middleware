<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Factory;

use Laminas\Hydrator\HydratorInterface;
use Psr\Http\Message\RequestInterface;

interface MutateHydratorFactoryInterface
{
    public function getHydrator(string $objectClassName, array $data, HydratorInterface $hydrator = null): HydratorInterface;
}