<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

use Debiturio\HydratorMiddleware\Factory\ReadHydratorFactory;
use Debiturio\HydratorMiddleware\Property\PropertiesHydrator;
use Laminas\Hydrator\Strategy\CollectionStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ReadSingleHydratorMiddleware implements MiddlewareInterface
{
    /**
     * @param ReadHydratorFactory $hydratorFactory
     * @param PropertiesHydrator $propertiesHydrator
     * @param CaseStyle $caseStyle
     */
    public function __construct(
        private readonly ReadHydratorFactory $hydratorFactory,
        private readonly PropertiesHydrator  $propertiesHydrator,
        private readonly CaseStyle           $caseStyle = CaseStyle::CAMEL_CASE)
    {
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \ReflectionException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $params = $request->getQueryParams();
        $propertyData = array_key_exists('properties', $params) ? $params['properties'] : [];

        $collectionProperties = $this->propertiesHydrator->hydrate(
            is_string($propertyData) ? explode(',', $propertyData) : $propertyData,
        );

        $data = $request->getParsedBody();
        $objectClassName = $data ? get_class($data) : null;

        $hydrator = $objectClassName ?
            $this->hydratorFactory->getHydrator($objectClassName, $collectionProperties, $this->caseStyle) : null;

        $request = $hydrator ? $request->withParsedBody($hydrator->extract($data)) : $request;

        return $handler->handle($request);
    }

}