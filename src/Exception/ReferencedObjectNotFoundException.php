<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware\Exception;

class ReferencedObjectNotFoundException extends \Exception
{
    public function __construct(private readonly string $reference,
                                private readonly string $property,
                                int                     $code = 0,
                                ?\Throwable             $previous = null)
    {
        parent::__construct(
            sprintf('Reference %s not found for property %s.', $this->reference, $this->property),
            $code,
            $previous
        );
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }
}