<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

trait PropertyNameTrait
{
    public function convertCaseStyle(string $value, CaseStyle $inputCaseStyle, CaseStyle $outputCaseStyle): string
    {
        if ($inputCaseStyle === CaseStyle::CAMEL_CASE && $outputCaseStyle === CaseStyle::SNAKE_CASE) {
            return $this->camelCaseToSnakeCase($value);
        }

        if ($inputCaseStyle === CaseStyle::SNAKE_CASE && $outputCaseStyle === CaseStyle::CAMEL_CASE) {
            return $this->snakeCaseToCamelCase($value);
        }

        return $value;
    }

    // TODO remove deprecated
    /*public function getterNameToPropertyName(string $getterName, string $prefix = null): string
    {
        return $this->camelCaseToSnakeCase($this->getGetterNameWithoutPrefix($getterName,$prefix));
    }*/

    public function camelCaseToSnakeCase($str, $separator = "_"): string
    {
        if (empty($str)) {
            return $str;
        }
        $str = lcfirst($str);
        $str = preg_replace("/[A-Z]/", $separator . "$0", $str);
        return strtolower($str);
    }

    public function getGetterNameWithoutPrefix(string $name, string $prefix = 'get', bool $lcFirst = true): ?string
    {
        $name = str_starts_with($name, $prefix) ? substr($name, strlen($prefix)) : $name;
        return !$lcFirst ? $name : lcfirst($name);
    }

    public function snakeCaseToCamelCase(string $input, string $separator = '_'): string
    {
        return str_replace($separator, '', lcfirst(ucwords($input, $separator)));
    }
}