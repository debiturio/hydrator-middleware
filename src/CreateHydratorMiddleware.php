<?php
declare(strict_types=1);

namespace Debiturio\HydratorMiddleware;

use Debiturio\HydratorMiddleware\Factory\BaseHydratorFactoryInterface;
use Debiturio\HydratorMiddleware\Factory\MutateHydratorFactory;
use Laminas\Hydrator\ClassMethodsHydrator;
use Laminas\Hydrator\ReflectionHydrator;
use Laminas\Hydrator\Strategy\CollectionStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;

class CreateHydratorMiddleware extends AbstractHydrateMiddleware
{
    public function __construct(MutateHydratorFactory                         $hydratorFactory,
                                array                                         $endpointsToClasses,
                                private readonly BaseHydratorFactoryInterface $baseHydratorFactory)
    {
        parent::__construct($hydratorFactory, $endpointsToClasses);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws \ReflectionException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $data = $request->getParsedBody();

        $objectClassName = $this->getClassName($request);

        $hydrator = $this->hydratorFactory->getHydrator(
            $objectClassName,
            $data,
            $this->baseHydratorFactory->getHydratorFromRequest($request, ReflectionHydrator::class)
        );

        if ($this->dataIsCollection($data)) {
            $strategy = new CollectionStrategy($hydrator, $objectClassName);
            return $handler->handle($request->withParsedBody($strategy->hydrate($data, $data)));
        }

        $classReflection = new \ReflectionClass($objectClassName);

        if(!array_key_exists('id', $data) && property_exists($objectClassName, 'id')) {
            $data['id'] = Uuid::uuid4()->toString();
        }

        return $handler->handle(
            $request->withParsedBody($hydrator->hydrate($data, $classReflection->newInstanceWithoutConstructor()))
        );
    }
}